package com.example.finalexam.model;

import java.io.Serializable;

public class Account implements Serializable, Comparable {
    String accountNumber;
    String openDate;
    float balance;
    String name;
    String family;
    String phone;
    int sin;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getOpenDate() {
        return openDate;
    }

    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getSin() {
        return sin;
    }

    public void setSin(int sin) {
        this.sin = sin;
    }

    public Account(String accountNumber, String openDate, float balance, String name, String family, String phone, int sin) {
        this.accountNumber = accountNumber;
        this.openDate = openDate;
        this.balance = balance;
        this.name = name;
        this.family = family;
        this.phone = phone;
        this.sin = sin;
    }

    @Override
    public String toString() {
        return name + " " + family;
    }

    @Override
    public int compareTo(Object o) {
        Account otherAccount = (Account) o;
        return this.getSin() - otherAccount.getSin();
    }
}
