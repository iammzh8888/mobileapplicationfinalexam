package com.example.finalexam;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.finalexam.model.Account;

import java.io.Serializable;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements
        View.OnClickListener,
        AdapterView.OnItemClickListener,
        AdapterView.OnItemLongClickListener{

    final static int REQUEST_CODE1 = 1;
    final static int REQUEST_CODE2 = 2;
    Button buttonAdd;
    ArrayList<Account> accountsList;
    ArrayAdapter<Account> accountsAdapter;
    ListView listAccounts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
        initializeListView();
    }

    private void initialize() {
        buttonAdd = findViewById(R.id.buttonAdd);
        buttonAdd.setOnClickListener(this);
    }
    private void initializeListView() {

        // 1- Initialize ListView
        listAccounts = findViewById(R.id.list_Accounts);

        accountsList = new ArrayList<>();
        accountsList.add(new Account("R1254543","1999-01-01",500.33f, "Jerry", "Rat", "6325323", 1));
        accountsList.add(new Account("C7465423","2005-07-12",300.69f, "Tom","Cat","3243423432",2));
        accountsList.add(new Account("S7363444","2010-11-25",823.69f, "Mary","Smith","63256523", 3));

        // 2- Create an Adapter for ListView
        accountsAdapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                accountsList);

        // 3- Assign the Adapter to the list view
        listAccounts.setAdapter(accountsAdapter);

        // 5- Assign the listener to the list view
        listAccounts.setOnItemClickListener(this);
        listAccounts.setOnItemLongClickListener(this);
    }

    // View.OnClickListener ------------------------------------------------------------------------
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonAdd:
                gotoAddPage();
                break;
        }
    }

    public void gotoAddPage(){
        Bundle bundle = new Bundle();
        bundle.putSerializable("bundleExtra_accountsList", accountsList);
        bundle.putInt("accountId", -1);

        Intent intent = new Intent(this, ItemClickActivity.class);
        intent.putExtra("account_detail", bundle);
        startActivityForResult(intent, REQUEST_CODE1);
    }

    @Override
    public void onItemClick(AdapterView<?> listPeopleAdapterView,
                            View itemView,
                            int position,    // Position in the ListView start from zero
                            long id) {       // Row id of the underlying data

        Toast.makeText(MainActivity.this, "position = " +
                position + ", id = " + id,Toast.LENGTH_SHORT).show();

        Account selectedAccount = accountsList.get(position);

        // Pass the account the user clicks on to ItemClickActivity
        Bundle bundle = new Bundle();
        bundle.putSerializable("bundleExtra_accountsList", accountsList);
        bundle.putInt("accountId", position);

        Intent intent = new Intent(MainActivity.this, ItemClickActivity.class);
        intent.putExtra("account_detail", bundle);
        startActivityForResult(intent, REQUEST_CODE1);
    }

    // AdapterView.OnItemLongClickListener ---------------------------------------------------------
    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        // 'parent' The AdapterView where the click happened
        // 'position' is the index of selected item in the ListView

        // Pass the account the user clicks on to ItemClickActivity
        Bundle bundle = new Bundle();
        bundle.putSerializable("bundleExtra_accountsList", accountsList);
        bundle.putInt("accountId", position);

        Intent intent = new Intent(MainActivity.this, ItemLongClickActivity.class);
        intent.putExtra("account_detail", bundle);
        startActivityForResult(intent, REQUEST_CODE2);

        // true means that the View that currently received the event is the true event receiver and
        // the event should not be propagated to the other Views in the tree;
        // when you return false you let the event be passed to the other Views that may consume it.
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bundle bundle = data.getBundleExtra("return_result");
        Serializable returnList = bundle.getSerializable("return_bundleExtra");

        accountsList = (ArrayList<Account>)returnList;
        accountsAdapter.clear();
        accountsAdapter.addAll(accountsList);
        accountsAdapter.notifyDataSetInvalidated();
        listAccounts.setAdapter(accountsAdapter);
    }
}