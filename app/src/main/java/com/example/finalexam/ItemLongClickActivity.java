package com.example.finalexam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.finalexam.model.Account;

import java.util.ArrayList;

public class ItemLongClickActivity extends AppCompatActivity implements View.OnClickListener {

    TextView textBalance, textAmount;
    Button btnWithdraw;
    int id;
    ArrayList<Account> accountsList;
    Account account;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_long_click);
        initialize();
        getMyIntent();
        populateData(account);
    }

    private void initialize() {
        textBalance = findViewById(R.id.textBalance);
        textAmount = findViewById(R.id.textAmount);

        btnWithdraw = findViewById(R.id.btnWithdraw);
        btnWithdraw.setOnClickListener(this);
    }

    private void getMyIntent() {

        // 1- Get the drink from the intent
        accountsList = (ArrayList<Account>) getIntent().getBundleExtra("account_detail").getSerializable("bundleExtra_accountsList");
        id = (int) getIntent().getBundleExtra("account_detail").getSerializable("accountId");
        account = accountsList.get(id);
    }

    private void populateData(Account account) {
        if (account != null) {
            // 1- Populate the account number
            textBalance.setText(String.valueOf(account.getBalance()));
        }
    }

    // View.OnClickListener ------------------------------------------------------------------------
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnWithdraw:
                gotoWithdraw();
                break;
        }
    }

    private void gotoWithdraw() {
        String strAmount = textAmount.getText().toString();
        if (!strAmount.isEmpty()) {
            float amount = Float.parseFloat(strAmount);
            Account updateAccount = accountsList.get(id);
            if (amount <= updateAccount.getBalance()) {
                float left = updateAccount.getBalance() - amount;
                updateAccount.setBalance(left);
                accountsList.set(id, updateAccount);
                textBalance.setText(String.valueOf(left));
            } else {
                Toast.makeText(this,
                        "Balance is not enough.",
                        Toast.LENGTH_SHORT).show();
                return;
            }
        }
    }

    @Override
    public void onBackPressed() {
        Bundle bundle = new Bundle();
        bundle.putSerializable("return_bundleExtra",accountsList);
        Intent intent = new Intent();
        intent.putExtra("return_result",bundle);

        setResult(RESULT_OK,intent);
        finish();
    }
}