package com.example.finalexam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.finalexam.model.Account;

import java.util.ArrayList;

public class ItemClickActivity extends AppCompatActivity implements View.OnClickListener {

    EditText textAccountNumber, textOpenDate, textBalance, textName, textFamily, textPhone, textSin;
    Button btnAdd, btnFind, btnRemove, btnUpdate, btnSave, btnLoad, btnClear, btnShowAll;
    int id;
    ArrayList<Account> accountsList;
    Account account;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_click);
        initialize();
        getMyIntent();
        populateData(account);
    }

    private void initialize() {
        textAccountNumber = findViewById(R.id.textAccountNumber);
        textOpenDate = findViewById(R.id.textOpenDate);
        textBalance = findViewById(R.id.textBalance);
        textName = findViewById(R.id.textName);
        textFamily = findViewById(R.id.textFamily);
        textPhone = findViewById(R.id.textPhone);
        textSin = findViewById(R.id.textSin);

        btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);
        btnFind = findViewById(R.id.btnFind);
        btnFind.setOnClickListener(this);
        btnRemove = findViewById(R.id.btnRemove);
        btnRemove.setOnClickListener(this);
        btnUpdate = findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(this);
        btnSave = findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);
        btnLoad = findViewById(R.id.btnLoad);
        btnLoad.setOnClickListener(this);
        btnClear = findViewById(R.id.btnClear);
        btnClear.setOnClickListener(this);
        btnShowAll = findViewById(R.id.btnShowAll);
        btnShowAll.setOnClickListener(this);
    }

    private void getMyIntent() {

        // 1- Get the drink from the intent
        accountsList = (ArrayList<Account>) getIntent().getBundleExtra("account_detail").getSerializable("bundleExtra_accountsList");
        int tempId = getIntent().getBundleExtra("account_detail").getInt("accountId");
        if(tempId != -1){
            id = (int) getIntent().getBundleExtra("account_detail").getInt("accountId");
            account = accountsList.get(id);
        }
    }

    private void populateData(Account account) {
        if (account != null) {
            // 1- Populate the account number
            textAccountNumber.setText(account.getAccountNumber());

            // 2- Populate the open date
            textOpenDate.setText(account.getOpenDate());

            // 3- Populate the open date
            textBalance.setText(String.valueOf(account.getBalance()));

            // 4- Populate the person name
            textName.setText(account.getName());

            // 5- Populate the family name
            textFamily.setText(account.getFamily());

            // 6- Populate the phone
            textPhone.setText(account.getPhone());

            // 7- Populate the sin
            textSin.setText(String.valueOf(account.getSin()));
        }
    }

    // View.OnClickListener ------------------------------------------------------------------------
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAdd:
                gotoAdd();
                break;
            case R.id.btnFind:
                gotoFind();
                break;
            case R.id.btnRemove:
                gotoRemove();
                break;
            case R.id.btnUpdate:
                gotoUpdate();
                break;
            case R.id.btnSave:
                gotoSave();
                break;
            case R.id.btnLoad:
                gotoLoad();
                break;
            case R.id.btnClear:
                gotoClear();
                break;
            case R.id.btnShowAll:
                gotoShowAll();
                break;
        }
    }

    private int accountIsFoundBySin(int sin) {
        for (Account oneAccount : accountsList) {
            if (oneAccount.getSin() == sin) {
                return accountsList.indexOf(oneAccount);
            }
        }
        return -1;
    }

    private boolean accountIsFoundByAccountNumber(String accountNum) {
        boolean find = false;
        for (Account oneAccount : accountsList) {
            if (oneAccount.getAccountNumber() == accountNum) {
                find = true;
                break;
            }
        }
        return find;
    }

    private void gotoAdd() {
        String accountNumber = textAccountNumber.getText().toString();
        String openDate = textOpenDate.getText().toString();
        String strBalance = textBalance.getText().toString();
        String name = textName.getText().toString();
        String family = textFamily.getText().toString();
        String phone = textPhone.getText().toString();
        String strSin = textSin.getText().toString();

        if (!accountNumber.isEmpty() && !openDate.isEmpty() && !strBalance.isEmpty() && !name.isEmpty() && !family.isEmpty() && !phone.isEmpty() && !strSin.isEmpty()) {
            float balance = Float.parseFloat(strBalance);
            int sin = Integer.parseInt(strSin);

            Account newAccount = new Account(accountNumber, openDate, balance, name, family, phone, sin);

            if (accountIsFoundBySin(sin) != -1) {
                Toast.makeText(this,
                        "SIN " + strSin + " has already existed. Please enter a different number.",
                        Toast.LENGTH_SHORT).show();
                return;
            }

            if (accountIsFoundByAccountNumber(accountNumber)) {
                Toast.makeText(this,
                        "Account number " + accountNumber + " has already existed. Please enter a different number.",
                        Toast.LENGTH_SHORT).show();
                return;
            }

            accountsList.add(newAccount);
            gotoClear();
        } else
            Toast.makeText(this,
                    "Account Number or Open Date or Balance or Name or Family or Phone or SIN is empty.",
                    Toast.LENGTH_SHORT).show();
    }

    private void gotoFind() {
        String strSin = textSin.getText().toString();
        if (!strSin.isEmpty()) {
            int sin = Integer.parseInt(strSin);
            int index = accountIsFoundBySin(sin);
            if(index != -1){
                Account oneAccount = accountsList.get(index);
                textAccountNumber.setText(oneAccount.getAccountNumber());
                textOpenDate.setText(oneAccount.getOpenDate());
                textBalance.setText(String.valueOf(oneAccount.getBalance()));
                textName.setText(oneAccount.getName());
                textFamily.setText(oneAccount.getFamily());
                textPhone.setText(oneAccount.getPhone());
                textSin.setText(String.valueOf(oneAccount.getSin()));
            }
            else{
                Toast.makeText(this,
                        "SIN " + strSin + " can't be found.",
                        Toast.LENGTH_SHORT).show();
                return;
            }
        } else
            Toast.makeText(this,
                    "SIN is empty.",
                    Toast.LENGTH_SHORT).show();
    }

    private void gotoRemove() {
        String strSin = textSin.getText().toString();
        if (!strSin.isEmpty()) {
            int sin = Integer.parseInt(strSin);
            int index = accountIsFoundBySin(sin);
            if(index != -1){
                accountsList.remove(index);
            }
            else{
                Toast.makeText(this,
                        "SIN " + strSin + " can't be found.",
                        Toast.LENGTH_SHORT).show();
                return;
            }
        } else
            Toast.makeText(this,
                    "SIN is empty.",
                    Toast.LENGTH_SHORT).show();
    }

    private void gotoUpdate() {
        String accountNumber = textAccountNumber.getText().toString();
        String openDate = textOpenDate.getText().toString();
        String strBalance = textBalance.getText().toString();
        String name = textName.getText().toString();
        String family = textFamily.getText().toString();
        String phone = textPhone.getText().toString();
        String strSin = textSin.getText().toString();

        if (!accountNumber.isEmpty() && !openDate.isEmpty() && !strBalance.isEmpty() && !name.isEmpty() && !family.isEmpty() && !phone.isEmpty() && !strSin.isEmpty()) {
            float balance = Float.parseFloat(strBalance);
            int sin = Integer.parseInt(strSin);

            int index = accountIsFoundBySin(sin);
            if(index != -1){
                Account updateAccount = accountsList.get(index);
                updateAccount.setAccountNumber(accountNumber);
                updateAccount.setOpenDate(openDate);
                updateAccount.setBalance(balance);
                updateAccount.setName(name);
                updateAccount.setFamily(family);
                updateAccount.setPhone(phone);
                updateAccount.setSin(sin);
                accountsList.set(index, updateAccount);
            }
            else{
                Toast.makeText(this,
                        "SIN " + strSin + " can't be found.",
                        Toast.LENGTH_SHORT).show();
                return;
            }
        } else
            Toast.makeText(this,
                    "Account Number or Open Date or Balance or Name or Family or Phone or SIN is empty.",
                    Toast.LENGTH_SHORT).show();
    }

    private void gotoSave() {
        String accountNumber = textAccountNumber.getText().toString();
        String openDate = textOpenDate.getText().toString();
        String strBalance = textBalance.getText().toString();
        String name = textName.getText().toString();
        String family = textFamily.getText().toString();
        String phone = textPhone.getText().toString();
        String strSin = textSin.getText().toString();

        if (!accountNumber.isEmpty() && !openDate.isEmpty() && !strBalance.isEmpty() && !name.isEmpty() && !family.isEmpty() && !phone.isEmpty() && !strSin.isEmpty()) {
            float balance = Float.parseFloat(strBalance);
            int sin = Integer.parseInt(strSin);

            int index = accountIsFoundBySin(sin);
            if(index != -1){
                Account updateAccount = accountsList.get(index);
                updateAccount.setAccountNumber(accountNumber);
                updateAccount.setOpenDate(openDate);
                updateAccount.setBalance(balance);
                updateAccount.setName(name);
                updateAccount.setFamily(family);
                updateAccount.setPhone(phone);
                updateAccount.setSin(sin);
                accountsList.set(index, updateAccount);
            }
            else{
                Toast.makeText(this,
                        "SIN " + strSin + " can't be found.",
                        Toast.LENGTH_SHORT).show();
                return;
            }
        } else
            Toast.makeText(this,
                    "Account Number or Open Date or Balance or Name or Family or Phone or SIN is empty.",
                    Toast.LENGTH_SHORT).show();
    }

    private void gotoLoad() {
        String strSin = textSin.getText().toString();
        if (!strSin.isEmpty()) {
            int sin = Integer.parseInt(strSin);
            int index = accountIsFoundBySin(sin);
            if(index != -1){
                Account oneAccount = accountsList.get(index);
                textAccountNumber.setText(oneAccount.getAccountNumber());
                textOpenDate.setText(oneAccount.getOpenDate());
                textBalance.setText(String.valueOf(oneAccount.getBalance()));
                textName.setText(oneAccount.getName());
                textFamily.setText(oneAccount.getFamily());
                textPhone.setText(oneAccount.getPhone());
                textSin.setText(String.valueOf(oneAccount.getSin()));
            }
            else{
                Toast.makeText(this,
                        "SIN " + strSin + " can't be found.",
                        Toast.LENGTH_SHORT).show();
                return;
            }
        } else
            Toast.makeText(this,
                    "SIN is empty.",
                    Toast.LENGTH_SHORT).show();
    }

    private void gotoClear() {
        textAccountNumber.setText("");
        textOpenDate.setText("");
        textBalance.setText("");
        textName.setText("");
        textFamily.setText("");
        textPhone.setText("");
        textSin.setText("");

        textName.requestFocus();
    }

    private void gotoShowAll() {
        Bundle bundle = new Bundle();
        bundle.putSerializable("return_bundleExtra", accountsList);

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("return_result", bundle);

        //------------------------------------ Set Result for MainActivity
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        Bundle bundle = new Bundle();
        bundle.putSerializable("return_bundleExtra", accountsList);

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("return_result", bundle);

        //------------------------------------ Set Result for MainActivity
        setResult(RESULT_OK, intent);
        finish();
    }
}